CODE REFERENCE FROM INTERNET: 
1. Android Toast Native 
2. Location Fetch.


ARCHITECTURE :
1. ToastManager (Signleton) (SDK) : In this design pattern, only a single class is involved, and it creates a single object. Even if multiple instances are requested, it will return the same object.


USAGE OF SDK :
1. Import the Unity Package (Toast.unitypackage) from the following link , also contains apk in same folder: https://drive.google.com/drive/folders/1DNDjidXa9j0lmxk1ARoJrA5p7acqz6rv?usp=sharing
2. After importing, drag and drop the prefab into the Unity hierarchy at"Assets/Toast/Prefab/ToastPrefab.prefab"
3. Use any manager(script) to control the ToastManager script. Set the toast message using 
ToastManager.Instance.ToastMsg = "ABC";
When a game object (Rotating Cube) is clicked, it will display the assigned toast on Android.
4. The manager class can control the ToastManager's cube color during loading, failure, and success states for permission, location fetch, and weather fetch.
5. Clicking on the game object will show a toast at any state, and the respective message will be displayed.
6. Red color signifies a failure of permission, location, or weather data. Yellow indicates loading, and green represents the success of the final weather state and temperature data set to ToastMsg.



TESTING FRAMEWORK :
1. Unit Testing (PlayMode) : A single method is created to test the weather flow from the API hit to data fetch and temperature setting. In the Unity editor, Toasts cannot be seen, so debug.log is used for debugging. Location permission is skipped in the editor or play mode test. Location fetch errors are handled in the editor, and static variables are used for latitude and longitude.
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.Networking;

public class WeatherManager : MonoBehaviour
{
    public int WeatherFetchTimeout = 10;

    #region "Init"
    void Start()
    {
#if UNITY_EDITOR
        StartCoroutine(LocationCoroutine());
#elif UNITY_ANDROID
        WaitForPremissionAndroid();
#endif
    }

    #endregion

    #region "Permission Methods and Callback"
    internal void PermissionCallbacks_PermissionDeniedAndDontAskAgain(string permissionName)
    {
        ToastManager.Instance.Fail();
        ToastManager.Instance.ToastMsg = WeatherConst.locationPermissionNotGranted;
    }

    internal void PermissionCallbacks_PermissionGranted(string permissionName)
    {
        StartCoroutine(LocationCoroutine());
    }

    internal void PermissionCallbacks_PermissionDenied(string permissionName)
    {
        ToastManager.Instance.Fail();
        ToastManager.Instance.ToastMsg = WeatherConst.locationPermissionNotGranted;
    }
     private void PermissionCallHandle()
    {
        var callbacks = new PermissionCallbacks();
        Permission.RequestUserPermission(Permission.CoarseLocation, callbacks);
        callbacks.PermissionDenied += PermissionCallbacks_PermissionDenied;
        callbacks.PermissionGranted += PermissionCallbacks_PermissionGranted;
        callbacks.PermissionDeniedAndDontAskAgain += PermissionCallbacks_PermissionDeniedAndDontAskAgain;
    }   
    private void WaitForPremissionAndroid()
    {
        if (Permission.HasUserAuthorizedPermission(Permission.CoarseLocation))
        {
            StartCoroutine(LocationCoroutine());
        }
        else
        {
            PermissionCallHandle();
        }
    }
    #endregion   

    #region "Location Fetch"
    IEnumerator LocationCoroutine()
    {
        ToastManager.Instance.Loading();
        ToastManager.Instance.ToastMsg = WeatherConst.loadingLocationStr;

#if UNITY_EDITOR
        // No permission handling needed in Editor
#elif UNITY_ANDROID

        // First, check if user has location service enabled
        if (!UnityEngine.Input.location.isEnabledByUser) {
           ToastManager.Instance.ToastMsg = WeatherConst.loadingLocationFailedStr;
           ToastManager.Instance.Fail();
            Debug.LogFormat("Android and Location not enabled");
            yield break;
        }

#elif UNITY_IOS
        if (!UnityEngine.Input.location.isEnabledByUser) {
           ToastManager.Instance.ToastMsg = WeatherConst.loadingLocationFailedStr;
           ToastManager.Instance.Fail();
            Debug.LogFormat("IOS and Location not enabled");
            yield break;
        }
#endif
        // Start service before querying location
        Input.location.Start(500f, 500f);

        // Wait until service initializes
        int maxWait = WeatherFetchTimeout;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSecondsRealtime(1);
            maxWait--;
        }

        // adding static variable for editor
#if UNITY_EDITOR
        WeatherInit("19.02", "72.87");
#endif

        // Service didn't initialize in given seconds
        if (maxWait < 1)
        {
            ToastManager.Instance.ToastMsg = WeatherConst.loadingLocationFailedStr;
            ToastManager.Instance.Fail();
            yield break;
        }

        // Connection has failed
        if (Input.location.status != LocationServiceStatus.Running)
        {
            ToastManager.Instance.ToastMsg = WeatherConst.loadingLocationStr;
            ToastManager.Instance.Fail();
            Debug.LogFormat("Unable to determine device location. Failed with status {0}", UnityEngine.Input.location.status);
            ToastManager.Instance.ToastMsg = WeatherConst.loadingLocationFailedStr;
            yield break;
        }
        else
        {
            var _latitude = Input.location.lastData.latitude;
            var _longitude = Input.location.lastData.longitude;
            WeatherInit(_latitude.ToString(), _longitude.ToString());
        }

        Input.location.Stop();
    }

    #endregion

    #region "Weather Fetch"


    [HideInInspector] public bool weatherFetchSucess;
    [HideInInspector] public bool weatherFetchFail;
    public void SetTemperature(string fetchedWeatherStr)
    {
        WeatherData _data = JsonUtility.FromJson<WeatherData>(fetchedWeatherStr);
        ToastManager.Instance.ToastMsg = $"{_data.daily.temperature_2m_max[0]} {_data.daily_units.temperature_2m_max}";
    }
    public void WeatherInit(string _latitude, string _longitude) 
    {
        StartCoroutine(FetchWeatherData(_latitude: _latitude, _longitude: _longitude, _timezone: "IST"));
    }
    private IEnumerator FetchWeatherData(string _latitude, string _longitude, string _timezone)
    {
        ToastManager.Instance.ToastMsg = WeatherConst.loadingWeatherStr;
        string url = WeatherConst.weatherEndBase + WeatherConst.GetParameter(latitude: _latitude, longitude: _longitude, timezone: _timezone);       
        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            yield return request.SendWebRequest();
            try
            {
                if (request.result == UnityWebRequest.Result.ConnectionError)
                {
                    Debug.Log(request.error);
                    ToastManager.Instance.Fail();
                    weatherFetchFail = true;
                    ToastManager.Instance.ToastMsg = request.error;
                }
                else
                {
                    ToastManager.Instance.Success();
                    weatherFetchSucess = true;
                    SetTemperature(request.downloadHandler.text);
                }
            }
            catch (Exception e)
            {
                ToastManager.Instance.Fail();
                ToastManager.Instance.ToastMsg = WeatherConst.loadingWeatherFailedStr;
                Debug.LogError("Error found in data fetch " + e.Message);
            }
        }        
    }


    #endregion
}





class WeatherConst
{
    public static string weatherEndBase = "https://api.open-meteo.com/v1/forecast?";
    public static string loadingWeatherStr = "Fetching weather data...";
    public static string loadingWeatherFailedStr = "Weather data fail to load";
    public static string loadingLocationFailedStr = "Fetching location Failed, Try Again, Relaunch";
    public static string loadingLocationStr = "Fetching location data...";
    public static string locationPermissionNotGranted = "Location Access Permission Failed";
    // latitude=19.07&longitude=72.87&timezone=IST&daily=temperature_2m_max
    public static string GetParameter(string latitude, string longitude, string timezone) {

        string _parm = $"latitude={latitude}&longitude={longitude}&timezone={timezone}&daily=temperature_2m_max";
        return _parm;
    }

}
[System.Serializable]
class WeatherData
{
    public float latitude;
    public float longitude;
    public float generationtime_ms;
    public int utc_offset_seconds;
    public string timezone;
    public string timezone_abbreviation;
    public int elevation;
    public DailyUnits daily_units;
    public DailyUnitsArr daily;
}
[System.Serializable]
class DailyUnits
{
    public string time;
    public string temperature_2m_max;
}
[System.Serializable]
class DailyUnitsArr
{
    public string[] time;
    public float[] temperature_2m_max;
}


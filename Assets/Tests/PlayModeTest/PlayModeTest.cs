using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

public class PlayModeTest
{
   
    [UnityTest]
    public IEnumerator WeatherInit()
    {
        // Setup
       
        var toastObj = new GameObject();
        ToastManager toast = toastObj.AddComponent<ToastManager>();

        MeshRenderer mesh = toastObj.AddComponent<MeshRenderer>();
        toast.mesh = mesh;
        toast.successColor = new Color(0, 1, 0, 1); //green
        toast.failColor = new Color(1, 0, 0, 1); //red
        toast.loadingColor = new Color(1, 1, 0, 1);//yellow

        var weatherObj = new GameObject();
        WeatherManager manager = weatherObj.AddComponent<WeatherManager>();

        Assert.IsNotNull(manager);
        Assert.IsNotNull(toast);        
        Assert.IsNotNull(mesh);
        yield return new WaitForEndOfFrame();
        // setup end

        // permission for location is skipped in editor
        // location fetch will fail in editor , only giving logs
        // added static parameter for latitude and longitude in WeatherInit for Editor, hence may give same result.
        // api will be checked in this test.

        // showing toast check if userclick gameobject before fetch, for testing purpose, in editor it will log the toast message
        toast.ShowToastMessage();


        yield return new WaitUntil( () => (manager.weatherFetchFail == true || (manager.weatherFetchSucess == true)));

        // if fail , test will fail
        Assert.AreEqual(true, manager.weatherFetchSucess);

        // checking gameobject color update
        Assert.AreEqual(toast.successColor, toast.mesh.material.color);

        // showing toast check if userclick gameobject after fetch, for testing purpose, in editor it will log the toast message
        toast.ShowToastMessage();

        yield return null;
    }
}
